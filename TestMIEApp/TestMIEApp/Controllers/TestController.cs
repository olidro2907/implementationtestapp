﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestMIEApp.Models;

namespace TestMIEApp.Controllers
{
    public class TestController : Controller
    {
        private readonly ITestRepository _testRepository;
        public TestController(ITestRepository testRepository)
        {
            _testRepository = testRepository;
        }
        public IActionResult Start()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Start(PersonalData post_person)
        {
            if (ModelState.IsValid)
            {
                Instruction(post_person);
                return RedirectToAction("Instruction");
            }
            else
                return View(post_person);
        }

        public IActionResult Instruction(PersonalData person)
        {
            var test = _testRepository.DownloadTest(person);
            return View(test);
        }
        public IActionResult Test(PersonalData person)
        {
            var test = _testRepository.DownloadTest(person);
            return View(test);
        }

    }
}