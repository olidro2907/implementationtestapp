﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace TestMIEApp.Models
{
    public class Definitions
    {
        public string [] A = { null,
            "blaming \n\"The policeman was accusing the man of stealing a wallet.\"",
            "showing fondness towards someone \n\"Most mothers are affectionate to their babies by giving them lots of kisses and cuddles.\"",
            "horrified, astonished, alarmed\n\"Jane was aghast when she discovered her house had been burgled.\"",
            "fearful, worried, filled with anxiety \n\"Claire was alarmed when she thought she was being followed home.\"",
            "finding something funny \n\"I was amused by a funny joke someone told me.\"",
            null,
            "irritated, displeased \n\"Jack was annoyed when he found out he had missed the last bus home.\"",
            "expecting \n\"At the start of the football match, the fans were anticipating a quick goal.\"",
            "worried, tense, uneasy \n\"The student was feeling anxious before taking her final exams.\"",
            "feeling sorry \n\"The waiter was very apologetic when he spilt soup all over the customer.\"",
            "conceited, self-important, having a big opinion of oneself \n\"The arrogant man thought he knew more about politics than everyone else in the room.\"",
            "overcome with shame or guilt \n\"The boy felt ashamed when his mother discovered him stealing money from her purse.\"" };
        public string [] B = { "confused, puzzled, dumbfounded \n\"The detectives were completely baffled by the murder case.\"",
            "utterly confused, puzzled, dazed \n\"The child was bewildered when visiting the big city for the first time.\"",
            null,
            null };
        public string [] C = { "careful, wary \n\"Sarah was always a bit cautious when talking to someone she did not know.\"",
            "consoling, compassionate \n\"The nurse was comforting the wounded soldier.\"",
            "worried, troubled \n\"The doctor was concerned when his patient took a turn for the worse.\"",
            "self-assured, believing in oneself \n\"The tennis player was feeling very confident about winning his match.\"",
            "puzzled, perplexed \n\"Lizzie was so confused by the directions given to her, she got lost.\"",
            "reflective, thoughtful, considering \n\"John was in a contemplative mood on the eve of his 60th birthday.\"",
            "satisfied \n\"After a nice walk and a good meal, David felt very contented.\"",
            "certain, absolutely positive \n\"Richard was convinced he had come to the right decision.\"",
            null,
            "inquisitive, inquiring, prying \n\"Louise was curious about the strange shaped parcel.\"" };
        public string[] D = { null,
            "already made your mind up \n\"Jane looked very decisive as she walked into the polling station.\"",
            "insolent, bold, dont care what anyone else thinks \n\"The animal protester remained defiant even after being sent to prison.\"",
            "miserable \n\"George was depressed when he didn't receive any birthday cards.\"",
            "passion, lust, longing for \n\"Kate had a strong desire for chocolate.\"",
            "gloomy, despairing, without hope \n\"Gary was despondent when he did not get the job he wanted.\"",
            "displeased, disgruntled \n\"Manchester United fans were disappointed not to win the Championship.\"",
            null,
            null,
            "glum, miserable, low \n\"Adam was dispirited when he failed his exams.\"",
            "suspicious, doubtful, wary \n\"The old woman was distrustful of the stranger at her door.\"",
            "commanding, bossy \n\"The sergeant major looked dominant as he inspected the new recruits.\"",
            "dubious, suspicious, not really believing \n\"Mary was doubtful that her son was telling the truth.\"" };
        public string[] E = { "ashamed \n\"After forgetting a colleague's name, Jenny felt very embarrassed.\"",
            "hopeful, heartening, supporting \n\"All the parents were encouraging their children in the school sports day.\"",
            null };
        public string[] F = { "daydreaming \n\"Emma was fantasizing about being a film star.\"",
            null,
            "brazen, saucy, teasing, playful \n\"Connie was accused of being flirtatious when she winked at a stranger at a party.\"",
            "confused, nervous and upset \n\"Sarah felt a bit flustered when she realised how late she was for the meeting and that she had forgotten an important document.\"",
            "sociable, amiable \n\"The friendly girl showed the tourists the way to the town centre.\"" };
        public string[] G = { "thankful \n\"Kelly was very grateful for the kindness shown by the stranger.\"",
            "feeling sorry for doing something wrong \n\"Charlie felt guilty about having an affair.\"" };
        public string[] H = { null,
            null,
            "showing intense dislike \n\"The two sisters were hateful to each other and always fighting.\"",
            null,
            "terrified, appalled \n\"The man was horrified to discover that his new wife was already married.\"",
            "unfriendly \n\"The two neighbours were hostile towards each other because of an argument about loud music.\"" };
        public string[] I = { "restless, wanting something to happen soon \n\"Jane grew increasingly impatient as she waited for her friend who was already 20 minutes late.\"",
            "begging, pleading \n\"Nicola looked imploring as she tried to persuade her dad to lend her the car.\"",
            "not believing \n\"Simon was incredulous when he heard that he had won the lottery.\"",
            "unsure, hesitant, unable to make your mind up \n\"Tammy was so indecisive that she couldn't even decide what to have for lunch.\"",
            "disinterested, unresponsive, don't care \n\"Terry was completely indifferent as to whether they went to the cinema or the pub.\"",
            "demanding, persisting, maintaining \n\"After a work outing, Frank was insisting he paid the bill for everyone.\"",
            "inquiring, curious \n\"After seeing Jurassic Park, Hugh grew very interested in dinosaurs.\"",
            "exasperated, annoyed \n\"Frances was irritated by all the junk mail she received.\"" };
        public string[] J = { "envious\n\"Tony was jealous of all the taller, better-looking boys in his class.\"",
            "being funny, playful \n\"Gary was always joking with his friends.\"" };
        public string[] K = { null };
        public string[] M = { "made up her mind", "making somebody do something" };
        public string[] N = { "apprehensive, tense, worried \n\"Just before her job interview, Alice felt very nervous.\"",
            null,
            null};
        public string[] P = { "distraught, feeling of terror or anxiety \n\"On waking to find the house on fire, the whole family was panicked.\"",
            "thinking about something slightly worrying \n\"Susie looked pensive on the way to meeting her boyfriend's parents for the first time.\"",
            "full of high spirits and fun \n\"Neil was feeling playful at his birthday party.\"",
            null,
            "absorbed, engrossed in one's own thoughts \n\"Worrying about her mother's illness made Debbie preoccupied at work.\"",
            "perplexed, bewildered, confused \n\"After doing the crossword for an hour, June was still puzzled by one clue.\"" };
        public string[] R = { "supporting, encouraging, giving someone confidence \n\"Andy tried to look reassuring as he told his wife that her new dress did suit her.\"",
            "contemplative, thoughtful \n\"George was in a reflective mood as he thought about what he'd done with his life.\"",
            "sorry \n\"Lee was always regretful that he had never travelled when he was younger.\"",
            "taking it easy, calm, carefree \n\"On holiday, Pam felt happy and relaxed.\"",
            "freed from worry or anxiety \n\"At the restaurant, Ray was relieved to find that he had not forgotten his wallet.\"",
            "bitter, hostile \n\"The businessman felt very resentful towards his younger colleague who had been promoted above him.\"" };
        public string[] S = { null,
            "cynical, mocking, scornful \n\"The comedian made a sarcastic comment when someone came into the theatre late.\"",
            null,
            "doubtful, suspicious, mistrusting \n\"Patrick looked sceptical as someone read out his horoscope to him.\"",
            "solemn, grave \n\"The bank manager looked serious as he refused Nigel an overdraft.\"",
            null,
            "sorry",
            null,
            null,
            null,
            "disbelieving, suspecting, doubting \n\"After Sam had lost his wallet for the second time at work, he grew suspicious of one of his colleagues.\"",
            "kind, compassionate \n\"The nurse looked sympathetic as she told the patient the bad news.\"" };
        public string[] T = { "hesitant, uncertain, cautious \n\"Andrew felt a bit tentative as he went into the room full of strangers.\"",
            "alarmed, fearful \n\"The boy was terrified when he thought he saw a ghost.\"",
            null,
            "thinking about something \n\"Phil looked thoughtful as he sat waiting for the girlfriend he was about to finish with.\"",
            "menacing, intimidating \n\"The large, drunken man was acting in a very threatening way.\"" };
        public string[] U = { "unsettled, apprehensive, troubled \n\"Karen felt slightly uneasy about accepting a lift from the man she had only met that day.\"",
            null,
            "agitated, worried, uneasy \n\"The man was very upset when his mother died.\"" };
        public string[] W = { null,
            "anxious, fretful, troubled \n\"When her cat went missing, the girl was very worried.\"" };
    }
}
