﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;

namespace TestMIEApp.Models
{
    public class PersonalData
    {
        public int PersonalId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage ="Age is necessary. Please enter age.")]
        [Range(1,116)]
        public int? Age { get; set; }

        [Required(ErrorMessage = "Gender is necessary.")]
        [EnumDataType(typeof(Gender))]
        public Gender? PersonGender { get; set; }

        public enum Gender
        {
            Male = 1,
            Female = 2
        }

        //public PersonalData(int age)
        //{
        //       this.Age = age;
        //}
    }
}
