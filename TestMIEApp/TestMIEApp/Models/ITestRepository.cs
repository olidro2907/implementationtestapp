﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMIEApp.Models
{
    public interface ITestRepository
    {
        Test DownloadTest(PersonalData data);
    }
}
