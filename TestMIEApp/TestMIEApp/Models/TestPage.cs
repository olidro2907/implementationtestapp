﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMIEApp.Models
{
    public class TestPage
    {
        public int PageNumber { get; set; }
        public IEnumerable<FullAnswer> Answers { get; set; }
        public string PhotoUrl { get; set; }
        public string CorrectAnswer { get; set; }
        public string CheckAnswer { get; set; }
        
        public TestPage(int pageNumber, IEnumerable <FullAnswer> fullAnswers, string photoUrl, string correctanswer)
        {
            this.PageNumber = pageNumber;
            this.Answers = fullAnswers;
            this.PhotoUrl = photoUrl;
            this.CorrectAnswer = correctanswer;
        }
        public TestPage(int pageNumber)
        {
            this.PageNumber = pageNumber;    
        }

        
    }
}
