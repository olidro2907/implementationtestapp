﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMIEApp.Models
{
    public class MockTestRepository : ITestRepository
    {
        //private List<Test> childTests;
        //private List<Test> adultTests;
        private Test currentTest;
        public MockTestRepository()
        {
            
        }

        public Test DownloadTest(PersonalData data)
        {
            return GenerateTestForPerson(data); ;
        }

        private Test GenerateTestForPerson(PersonalData data)
        {
           Test test = new Test(data);
           Dictionary words = new Dictionary();
           Definitions explains = new Definitions();

            if (test.TestType == 'a')
            {
                FullAnswer[] answers = { new FullAnswer(words.J[0], explains.J[0]), new FullAnswer(words.P[0], explains.P[0]),
                    new FullAnswer(words.A[11], explains.A[11]), new FullAnswer(words.H[2], explains.H[2]) };
                test.Instruction = new TestPage(0, answers, "/images/adult/practice.JPG", answers[1].Answer);

                //for (int i = 0; i < test.TestsPages.Length; i++)
                //{
                //    switch (i)
                //    {
                //        case 0:
                //            string[] a1 = { words.P[2], words.C[1], words.I[7], words.B[2] };
                //            string[] d1 = { explains.P[2], explains.C[1], explains.I[7], explains.B[2] };
                //            test.TestsPages[i] = new TestPage(i + 1, a1, d1, "/images/adult/1.JPG", a1[0]);
                //            continue;
                //        case 1:
                //            string[] a2 = { words.T[1], words.U[2], words.A[11], words.A[7] };
                //            string[] d2 = { explains.T[1], explains.U[2], explains.A[11], explains.A[7] };
                //            test.TestsPages[i] = new TestPage(i + 1, a2, d2, "/images/adult/2.JPG", a2[1]);
                //            continue;
                //        case 2:
                //            string[] a3 = { words.J[1], words.F[3], words.D[4], words.C[7] };
                //            string[] d3 = { explains.J[1], explains.F[3], explains.D[4], explains.C[7] };
                //            test.TestsPages[i] = new TestPage(i + 1, a3, d3, "/images/adult/3.JPG", a3[2]);
                //            continue;
                //        case 3:
                //            string[] a4 = { words.J[1], words.I[5], words.A[5], words.R[3] };
                //            string[] d4 = { explains.J[1], explains.I[5], explains.A[5], explains.R[3] };
                //            test.TestsPages[i] = new TestPage(i + 1, a4, d4, "/images/adult/4.JPG", a4[1]);
                //            continue;
                //        case 4:
                //            string[] a5 = { words.I[7], words.S[1], words.W[1], words.F[4] };
                //            string[] d5 = { explains.I[7], explains.S[1], explains.W[1], explains.F[4] };
                //            test.TestsPages[i] = new TestPage(i + 1, a5, d5, "/images/adult/5.JPG", a5[2]);
                //            continue;
                //        case 5:
                //            string[] a6 = { words.A[3], words.F[0], words.I[0], words.A[4] };
                //            string[] d6 = { explains.A[3], explains.F[0], explains.I[0], explains.A[4] };
                //            test.TestsPages[i] = new TestPage(i + 1, a6, d6, "/images/adult/6.JPG", a6[1]);
                //            continue;
                //        case 6:
                //            string[] a7 = { words.A[10], words.F[4], words.U[0], words.D[9] };
                //            string[] d7 = { explains.A[10], explains.F[4], explains.U[0], explains.D[9] };
                //            test.TestsPages[i] = new TestPage(i + 1, a7, d7, "/images/adult/7.JPG", a7[2]);
                //            continue;
                //        case 7:
                //            string[] a8 = { words.D[5], words.R[4], words.W[1], words.S[5] };
                //            string[] d8 = { explains.D[5], explains.R[4], explains.W[1], explains.S[5] };
                //            test.TestsPages[i] = new TestPage(i + 1, a8, d8, "/images/adult/8.JPG", a8[0]);
                //            continue;
                //        case 8:
                //            string[] a9 = { words.A[7], words.H[5], words.H[4], words.P[4] };
                //            string[] d9 = { explains.A[7], explains.H[5], explains.H[4], explains.P[4] };
                //            test.TestsPages[i] = new TestPage(i + 1, a9, d9, "/images/adult/9.JPG", a9[3]);
                //            continue;
                //        case 9:
                //            string[] a10 = { words.C[0], words.I[5], words.B[2], words.A[3] };
                //            string[] d10 = { explains.C[0], explains.I[5], explains.B[2], explains.A[3] };
                //            test.TestsPages[i] = new TestPage(i + 1, a10, d10, "/images/adult/10.JPG", a10[0]);
                //            continue;
                //        case 10:
                //            string[] a11 = { words.T[1], words.A[5], words.R[2], words.F[2] };
                //            string[] d11 = { explains.T[1], explains.A[5], explains.R[2], explains.F[2] };
                //            test.TestsPages[i] = new TestPage(i + 1, a11, d11, "/images/adult/11.JPG", a11[2]);
                //            continue;
                //        case 11:
                //            string[] a12 = { words.I[4], words.E[0], words.S[3], words.D[9] };
                //            string[] d12 = { explains.I[4], explains.E[0], explains.S[3], explains.D[9] };
                //            test.TestsPages[i] = new TestPage(i + 1, a12, d12, "/images/adult/12.JPG", a12[2]);
                //            continue;
                //        case 12:
                //            string[] a13 = { words.D[1], words.A[5], words.A[3], words.B[2] };
                //            string[] d13 = { explains.D[1], explains.A[5], explains.A[3], explains.B[2] };
                //            test.TestsPages[i] = new TestPage(i + 1, a13, d13, "/images/adult/13.JPG", a13[1]);
                //            continue;
                //        case 13:
                //            string[] a14 = { words.I[7], words.D[6], words.D[3], words.A[1] };
                //            string[] d14 = { explains.I[7], explains.D[6], explains.D[3], explains.A[1] };
                //            test.TestsPages[i] = new TestPage(i + 1, a14, d14, "/images/adult/14.JPG", a14[3]);
                //            continue;
                //        case 14:
                //            string[] a15 = { words.C[5], words.F[3], words.E[1], words.A[5] };
                //            string[] d15 = { explains.C[5], explains.F[3], explains.E[1], explains.A[5] };
                //            test.TestsPages[i] = new TestPage(i + 1, a15, d15, "/images/adult/15.JPG", a15[0]);
                //            continue;
                //        case 15:
                //            string[] a16 = { words.I[7], words.T[3], words.E[1], words.S[11] };
                //            string[] d16 = { explains.I[7], explains.T[3], explains.E[1], explains.S[11] };
                //            test.TestsPages[i] = new TestPage(i + 1, a16, d16, "/images/adult/16.JPG", a16[1]);
                //            continue;
                //        case 16:
                //            string[] a17 = { words.D[12], words.A[2], words.P[2], words.A[3] };
                //            string[] d17 = { explains.D[12], explains.A[2], explains.P[2], explains.A[3] };
                //            test.TestsPages[i] = new TestPage(i + 1, a17, d17, "/images/adult/17.JPG", a17[0]);
                //            continue;
                //        case 17:
                //            string[] a18 = { words.D[1], words.A[5], words.A[3], words.B[2] };
                //            string[] d18 = { explains.D[1], explains.A[5], explains.A[3], explains.B[2] };
                //            test.TestsPages[i] = new TestPage(i + 1, a18, d18, "/images/adult/18.JPG", a18[0]);
                //            continue;
                //        case 18:
                //            string[] a19 = { words.A[11], words.G[0], words.S[1], words.T[0] };
                //            string[] d19 = { explains.A[11], explains.G[0], explains.S[1], explains.T[0] };
                //            test.TestsPages[i] = new TestPage(i + 1, a19, d19, "/images/adult/19.JPG", a19[3]);
                //            continue;
                //        case 19:
                //            string[] a20 = { words.D[12], words.F[4], words.G[1], words.H[4] };
                //            string[] d20 = { explains.D[12], explains.F[4], explains.G[1], explains.H[4] };
                //            test.TestsPages[i] = new TestPage(i + 1, a20, d20, "/images/adult/20.JPG", a20[1]);
                //            continue;
                //        case 20:
                //            string[] a21 = { words.E[0], words.F[0], words.C[4], words.P[0] };
                //            string[] d21 = { explains.E[0], explains.F[0], explains.C[4], explains.P[0] };
                //            test.TestsPages[i] = new TestPage(i + 1, a21, d21, "/images/adult/21.JPG", a21[1]);
                //            continue;
                //        case 21:
                //            string[] a22 = { words.P[4], words.G[0], words.I[5], words.I[1] };
                //            string[] d22 = { explains.P[4], explains.G[0], explains.I[5], explains.I[1] };
                //            test.TestsPages[i] = new TestPage(i + 1, a22, d22, "/images/adult/22.JPG", a22[0]);
                //            continue;
                //        case 22:
                //            string[] a23 = { words.C[6], words.A[10], words.D[2], words.C[9] };
                //            string[] d23 = { explains.C[6], explains.A[10], explains.D[2], explains.C[9] };
                //            test.TestsPages[i] = new TestPage(i + 1, a23, d23, "/images/adult/23.JPG", a23[2]);
                //            continue;
                //        case 23:
                //            string[] a24 = { words.P[1], words.I[7], words.E[2], words.H[5] };
                //            string[] d24 = { explains.P[1], explains.I[7], explains.E[2], explains.H[5] };
                //            test.TestsPages[i] = new TestPage(i + 1, a24, d24, "/images/adult/24.JPG", a24[0]);
                //            continue;
                //        case 24:
                //            string[] a25 = { words.P[0], words.I[2], words.D[5], words.I[6] };
                //            string[] d25 = { explains.P[0], explains.I[2], explains.D[5], explains.I[6] };
                //            test.TestsPages[i] = new TestPage(i + 1, a25, d25, "/images/adult/25.JPG", a25[3]);
                //            continue;
                //        case 25:
                //            string[] a26 = { words.A[4], words.S[5], words.H[5], words.A[9] };
                //            string[] d26 = { explains.A[4], explains.S[5], explains.H[5], explains.A[9] };
                //            test.TestsPages[i] = new TestPage(i + 1, a26, d26, "/images/adult/26.JPG", a26[2]);
                //            continue;
                //        case 26:
                //            string[] a27 = { words.J[1], words.C[0], words.A[11], words.R[0] };
                //            string[] d27 = { explains.J[1], explains.C[0], explains.A[11], explains.R[0] };
                //            test.TestsPages[i] = new TestPage(i + 1, a27, d27, "/images/adult/27.JPG", a27[1]);
                //            continue;
                //        case 27:
                //            string[] a28 = { words.I[6], words.J[1], words.A[2], words.C[6] };
                //            string[] d28 = { explains.I[6], explains.J[1], explains.A[2], explains.C[6] };
                //            test.TestsPages[i] = new TestPage(i + 1, a28, d28, "/images/adult/28.JPG", a28[0]);
                //            continue;
                //        case 28:
                //            string[] a29 = { words.I[0], words.A[3], words.I[7], words.R[1] };
                //            string[] d29 = { explains.I[0], explains.A[3], explains.I[7], explains.R[1] };
                //            test.TestsPages[i] = new TestPage(i + 1, a29, d29, "/images/adult/29.JPG", a29[3]);
                //            continue;
                //        case 29:
                //            string[] a30 = { words.G[0], words.F[2], words.H[5], words.D[6] };
                //            string[] d30 = { explains.G[0], explains.F[2], explains.H[5], explains.D[6] };
                //            test.TestsPages[i] = new TestPage(i + 1, a30, d30, "/images/adult/30.JPG", a30[1]);
                //            continue;
                //        case 30:
                //            string[] a31 = { words.A[12], words.C[3], words.J[1], words.D[9] };
                //            string[] d31 = { explains.A[12], explains.C[3], explains.J[1], explains.D[9] };
                //            test.TestsPages[i] = new TestPage(i + 1, a31, d31, "/images/adult/31.JPG", a31[1]);
                //            continue;
                //        case 31:
                //            string[] a32 = { words.S[4], words.A[12], words.B[1], words.A[4] };
                //            string[] d32 = { explains.S[4], explains.A[12], explains.B[1], explains.A[4] };
                //            test.TestsPages[i] = new TestPage(i + 1, a32, d32, "/images/adult/32.JPG", a32[0]);
                //            continue;
                //        case 32:
                //            string[] a33 = { words.E[0], words.G[1], words.F[0], words.C[2] };
                //            string[] d33 = { explains.E[0], explains.G[1], explains.F[0], explains.C[2] };
                //            test.TestsPages[i] = new TestPage(i + 1, a33, d33, "/images/adult/33.JPG", a33[3]);
                //            continue;
                //        case 33:
                //            string[] a34 = { words.A[3], words.B[0], words.D[10], words.T[1] };
                //            string[] d34 = { explains.A[3], explains.B[0], explains.D[10], explains.T[1] };
                //            test.TestsPages[i] = new TestPage(i + 1, a34, d34, "/images/adult/34.JPG", a34[2]);
                //            continue;
                //        case 34:
                //            string[] a35 = { words.P[5], words.N[0], words.I[5], words.C[5] };
                //            string[] d35 = { explains.P[5], explains.N[0], explains.I[5], explains.C[5] };
                //            test.TestsPages[i] = new TestPage(i + 1, a35, d35, "/images/adult/35.JPG", a35[1]);
                //            continue;
                //        case 35:
                //            string[] a36 = { words.A[12], words.N[0], words.S[10], words.I[3] };
                //            string[] d36 = { explains.A[12], explains.N[0], explains.S[10], explains.I[3] };
                //            test.TestsPages[i] = new TestPage(i + 1, a36, d36, "/images/adult/36.JPG", a36[2]);
                //            continue;
                //        default:
                //            break;
                //    }
                //}
                //adultTests.Add(test);
            }
            else if (test.TestType == 'c')
            {
                //string[] a0 = { words.J[0], words.S[2], words.R[3], words.H[1] };
                //string[] d0 = { explains.J[0], explains.S[2], explains.R[3], explains.H[1] };
                //test.Instruction = new TestPage(0, a0, d0, "/images/child/practice.JPG", a0[1]);

                //for (int i = 0; i < test.TestsPages.Length; i++)
                //    {
                //        switch (i)
                //        {
                //            case 0:
                //                string[] a1 = { words.H[1], words.S[9], words.K[0], words.C[8] };
                //                string[] d1 = { explains.H[1], explains.S[9], explains.K[0], explains.C[8] };
                //                test.TestsPages[i] = new TestPage(i + 1, a1, d1, "/images/child/1.JPG", a1[2]);
                //                continue;
                //            case 1:
                //                string[] a2 = { words.U[1], words.C[8], words.S[9], words.S[0] };
                //                string[] d2 = { explains.U[1], explains.C[8], explains.S[9], explains.S[0] };
                //                test.TestsPages[i] = new TestPage(i + 1, a2, d2, "/images/child/2.JPG", a2[3]);
                //                continue;
                //            case 2:
                //                string[] a3 = { words.F[4], words.S[0], words.S[9], words.W[1] };
                //                string[] d3 = { explains.F[4], explains.S[0], explains.S[9], explains.W[1] };
                //                test.TestsPages[i] = new TestPage(i + 1, a3, d3, "/images/child/3.JPG", a3[0]);
                //                continue;
                //            case 3:
                //                string[] a4 = { words.R[3], words.U[2], words.S[9], words.E[2] };
                //                string[] d4 = { explains.R[3], explains.U[2], explains.S[9], explains.E[2] };
                //                test.TestsPages[i] = new TestPage(i + 1, a4, d4, "/images/child/4.JPG", a4[1]);
                //                continue;
                //            case 4:
                //                string[] a5 = { words.F[1], words.M[1], words.J[1], words.R[3] };
                //                string[] d5 = { explains.F[1], explains.M[1], explains.J[1], explains.R[3] };
                //                test.TestsPages[i] = new TestPage(i + 1, a5, d5, "/images/child/5.JPG", a5[1]);
                //                continue;
                //            case 5:
                //                string[] a6 = { words.H[1], words.U[1], words.W[1], words.B[2] };
                //                string[] d6 = { explains.H[1], explains.U[1], explains.W[1], explains.B[2] };
                //                test.TestsPages[i] = new TestPage(i + 1, a6, d6, "/images/child/6.JPG", a6[2]);
                //                continue;
                //            case 6:
                //                string[] a7 = { words.F[1], words.B[2], words.I[6], words.J[1] };
                //                string[] d7 = { explains.F[1], explains.B[2], explains.I[6], explains.J[1] };
                //                test.TestsPages[i] = new TestPage(i + 1, a7, d7, "/images/child/7.JPG", a7[2]);
                //                continue;
                //            case 7:
                //                string[] a8 = { words.R[5], words.H[0], words.F[4], words.A[6] };
                //                string[] d8 = { explains.R[5], explains.H[0], explains.F[4], explains.A[6] };
                //                test.TestsPages[i] = new TestPage(i + 1, a8, d8, "/images/child/8.JPG", a8[0]);
                //                continue;
                //            case 8:
                //                string[] a9 = { words.A[7], words.H[1], words.S[9], words.T[2] };
                //                string[] d9 = { explains.A[7], explains.H[1], explains.S[9], explains.T[2] };
                //                test.TestsPages[i] = new TestPage(i + 1, a9, d9, "/images/child/9.JPG", a9[3]);
                //                continue;
                //            case 9:
                //                string[] a10 = { words.K[0], words.S[5], words.N[1], words.S[0] };
                //                string[] d10 = { explains.K[0], explains.S[5], explains.N[1], explains.S[0] };
                //                test.TestsPages[i] = new TestPage(i + 1, a10, d10, "/images/child/10.JPG", a10[2]);
                //                continue;
                //            case 10:
                //                string[] a11 = { words.B[3], words.H[3], words.A[6], words.D[8] };
                //                string[] d11 = { explains.B[3], explains.H[3], explains.A[6], explains.D[8] };
                //                test.TestsPages[i] = new TestPage(i + 1, a11, d11, "/images/child/11.JPG", a11[1]);
                //                continue;
                //            case 11:
                //                string[] a12 = { words.C[4], words.J[1], words.S[0], words.S[4] };
                //                string[] d12 = { explains.C[4], explains.J[1], explains.S[0], explains.S[4] };
                //                test.TestsPages[i] = new TestPage(i + 1, a12, d12, "/images/child/12.JPG", a12[3]);
                //                continue;
                //            case 12:
                //                string[] a13 = { words.T[2], words.U[2], words.E[2], words.H[0] };
                //                string[] d13 = { explains.T[2], explains.U[2], explains.E[2], explains.H[0] };
                //                test.TestsPages[i] = new TestPage(i + 1, a13, d13, "/images/child/13.JPG", a13[0]);
                //                continue;
                //            case 13:
                //                string[] a14 = { words.H[0], words.T[2], words.E[2], words.K[0] };
                //                string[] d14 = { explains.H[0], explains.T[2], explains.E[2], explains.K[0] };
                //                test.TestsPages[i] = new TestPage(i + 1, a14, d14, "/images/child/14.JPG", a14[1]);
                //                continue;
                //            case 14:
                //                string[] a15 = { words.N[1], words.F[4], words.W[0], words.R[3] };
                //                string[] d15 = { explains.N[1], explains.F[4], explains.W[0], explains.R[3] };
                //                test.TestsPages[i] = new TestPage(i + 1, a15, d15, "/images/child/15.JPG", a15[0]);
                //                continue;
                //            case 15:
                //                string[] a16 = { words.M[0], words.J[1], words.S[9], words.B[2] };
                //                string[] d16 = { explains.M[0], explains.J[1], explains.S[9], explains.B[2] };
                //                test.TestsPages[i] = new TestPage(i + 1, a16, d16, "/images/child/16.JPG", a16[0]);
                //                continue;
                //            case 16:
                //                string[] a17 = { words.A[6], words.F[4], words.U[1], words.A[0] };
                //                string[] d17 = { explains.A[6], explains.F[4], explains.U[1], explains.A[0] };
                //                test.TestsPages[i] = new TestPage(i + 1, a17, d17, "/images/child/17.JPG", a17[3]);
                //                continue;
                //            case 17:
                //                string[] a18 = { words.T[2], words.A[6], words.B[3], words.F[4] };
                //                string[] d18 = { explains.T[2], explains.A[6], explains.B[3], explains.F[4] };
                //                test.TestsPages[i] = new TestPage(i + 1, a18, d18, "/images/child/18.JPG", a18[0]);
                //                continue;
                //            case 18:
                //                string[] a19 = { words.A[6], words.D[0], words.S[0], words.I[6] };
                //                string[] d19 = { explains.A[6], explains.D[0], explains.S[0], explains.I[6] };
                //                test.TestsPages[i] = new TestPage(i + 1, a19, d19, "/images/child/19.JPG", a19[3]);
                //                continue;
                //            case 19:
                //                string[] a20 = { words.K[0], words.S[8], words.N[1], words.E[2] };
                //                string[] d20 = { explains.K[0], explains.S[8], explains.N[1], explains.E[2] };
                //                test.TestsPages[i] = new TestPage(i + 1, a20, d20, "/images/child/20.JPG", a20[2]);
                //                continue;
                //            case 20:
                //                string[] a21 = { words.I[6], words.J[1], words.R[3], words.H[0] };
                //                string[] d21 = { explains.I[6], explains.J[1], explains.R[3], explains.H[0] };
                //                test.TestsPages[i] = new TestPage(i + 1, a21, d21, "/images/child/21.JPG", a21[0]);
                //                continue;
                //            case 21:
                //                string[] a22 = { words.P[2], words.K[0], words.S[9], words.T[2] };
                //                string[] d22 = { explains.P[2], explains.K[0], explains.S[9], explains.T[2] };
                //                test.TestsPages[i] = new TestPage(i + 1, a22, d22, "/images/child/22.JPG", a22[3]);
                //                continue;
                //            case 22:
                //                string[] a23 = { words.S[9], words.S[7], words.J[1], words.H[0] };
                //                string[] d23 = { explains.S[9], explains.S[7], explains.J[1], explains.H[0] };
                //                test.TestsPages[i] = new TestPage(i + 1, a23, d23, "/images/child/23.JPG", a23[1]);
                //                continue;
                //            case 23:
                //                string[] a24 = { words.S[4], words.A[12], words.C[4], words.S[9] };
                //                string[] d24 = { explains.S[4], explains.A[12], explains.C[4], explains.S[9] };
                //                test.TestsPages[i] = new TestPage(i + 1, a24, d24, "/images/child/24.JPG", a24[0]);
                //                continue;
                //            case 24:
                //                string[] a25 = { words.S[5], words.G[1], words.D[0], words.W[1] };
                //                string[] d25 = { explains.S[5], explains.G[1], explains.D[0], explains.W[1] };
                //                test.TestsPages[i] = new TestPage(i + 1, a25, d25, "/images/child/25.JPG", a25[3]);
                //                continue;
                //            case 25:
                //                string[] a26 = { words.J[1], words.R[3], words.N[0], words.S[6] };
                //                string[] d26 = { explains.J[1], explains.R[3], explains.N[0], explains.S[6] };
                //                test.TestsPages[i] = new TestPage(i + 1, a26, d26, "/images/child/26.JPG", a26[2]);
                //                continue;
                //            case 26:
                //                string[] a27 = { words.A[12], words.E[2], words.N[1], words.P[3] };
                //                string[] d27 = { explains.A[12], explains.E[2], explains.N[1], explains.P[3] };
                //                test.TestsPages[i] = new TestPage(i + 1, a27, d27, "/images/child/27.JPG", a27[2]);
                //                continue;
                //            case 27:
                //                string[] a28 = { words.D[7], words.H[1], words.H[0], words.B[2] };
                //                string[] d28 = { explains.D[7], explains.H[1], explains.H[0], explains.B[2] };
                //                test.TestsPages[i] = new TestPage(i + 1, a28, d28, "/images/child/28.JPG", a28[2]);
                //                continue;
                //            default:
                //                break;
                //        }
                //}
                //childTests.Add(test);
            }
            return test;
        }
    }
}
