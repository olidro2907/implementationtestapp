﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMIEApp.Models
{
    public class Dictionary
    {
        public string [] A = { "a bit worried", "accusing", "affectionate", "aghast", "alarmed", "amused", "angry", "annoyed", "anticipating", "anxious", "apologetic", "arrogant", "ashamed" };
        public string [] B = { "baffled", "bewildered", "bored", "bossy" };
        public string [] C = { "cautious", "comforting", "concerned", "confident", "confused", "contemplative", "contented", "convinced", "cross", "curious" };
        public string [] D = { "daydreaming", "decisive", "defiant", "depressed", "desire", "despondent", "disappointed", "disgust", "disgusted", "dispirited", "distrustful", "dominant", "doubtful" };
        public string [] E = { "embarrassed", "encouraging", "excited" };
        public string[] F = { "fantasizing", "feeling sorry", "flirtatious", "flustered", "friendly" };
        public string[] G = { "grateful", "guilty" };
        public string[] H = { "happy", "hate", "hateful", "hoping", "horrified", "hostile" };
        public string[] I = { "impatient", "imploring", "incredulous", "indecisive", "indifferent", "insisting", "interested", "irritated" };
        public string[] J = { "jealous", "joking" };
        public string[] K = { "kind" };
        public string[] M = { "made up her mind", "making somebody do something" };
        public string[] N = { "nervous", "not believing", "not pleased" };
        public string[] P = { "panicked", "pensive", "playful", "pleased", "preoccupied", "puzzled" };
        public string[] R = { "reassuring", "reflective", "regretful", "relaxed", "relieved", "remembering" };
        public string[] S = { "sad", "sarcastic", "scared", "sceptical", "serious", "shy", "sorry", "sure about something", "surprise", "surprised", "suspicious", "sympathetic" };
        public string[] T = { "tentative", "terrified", "thinking about something", "thoughtful", "threatening" };
        public string[] U = { "uneasy", "unkind", "upset" };
        public string[] W = { "wanting to play", "worried" };



    }
}
