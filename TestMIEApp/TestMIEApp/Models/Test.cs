﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMIEApp.Models
{
    public class Test
    {
        public int TestId { get; set; }
        public char TestType { get; set; } //a-adult, c-child 
        public TestPage Instruction { get; set; }
        public TestPage [] TestsPages { get; set; }

        public Test(PersonalData person)
        {
            this.TestId = person.PersonalId;

            if (person.Age <=15)
            {
                this.TestType = 'c';
                this.TestsPages = new TestPage[28];
            }
            else
            {
                this.TestType = 'a';
                this.TestsPages = new TestPage[36];
            }
        } 
        
        //public Test(char testType)
        //{
        //    this.TestType = testType;
        //}

    }

    
}
