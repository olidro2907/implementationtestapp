﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMIEApp.Models
{
    public class FullAnswer
    {
        public string Answer { get; set; }
        public string Definition { get; set; }

        public FullAnswer(string answer, string definition)
        {
            this.Answer = answer;
            this.Definition = definition;
        }
    }
}
